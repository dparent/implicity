import axios from "axios";

const url: string = 'https://api.coindesk.com/v1/bpi/currentprice.json';

const api = async () => {
    try {
        let r = await axios.get(url);
        return r.data;
    } catch (exception) {
        process.stderr.write(`ERROR received from ${url}: ${exception}\n`);
    }
}

export { api };
