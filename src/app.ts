import * as Koa from 'koa';
import helmet from "koa-helmet";
import { api } from "./coindesk_api";


const app = new Koa();

// Provides important security headers to make your app more secure
// app.use(helmet());

app.use(async ctx => {
    let r = await api();
    ctx.body = "hello word";
});

async function getAsync1() {
    return 1; //https://api.coindesk.com/v1/bpi/currentprice.json
}

async function getAsync2() {
    return 2;
}

async function getAdditionAsynchrone() {
    const nb1 = await getAsync1();
    const nb2 = await getAsync2();
    return nb1 + nb2;
}

app.listen(process.env.PORT || 3000);
